//построитель таблицы
FinCalc.widgets.Table = function(options) {

	var cells = [];

	this.options = options = $.extend({
		headers : [],
		data : [[]],
		id : ''
	},options || {});

	this.element = document.createElement('div');
	this.element.id = options.id;

	this.init = function() {
		var headerLine = new FinCalc.widgets.TableLine({count : 4});
		headerLine.setParent(this);
		
		//создаем заголовки
		for(var i = 0; i < options.headers.length; i++) {
			var header = new FinCalc.widgets.Header({
				content:this.options.headers[i]
			});
			header.setParent(headerLine);
			header.render();
		}

		headerLine.render();

		//заполняем ячейки
		for(var i = 0; i < options.data.length; i++) {
			var line = new FinCalc.widgets.TableLine();
			line.setParent(this);
			
			cells[i] = [];

			for(var j = 0; j < options.data[i].length; j++) {
				var item = new FinCalc.widgets.Cell({
					content:options.data[i][j]
				});
				item.setParent(line);
				item.render();

				cells[i][j] = item;
			}

			line.render();
		}

	}

	this.getCells = function(i) {
		if(i !== undefined)
			return cells[i];
		
		return cells;
	}
	this.getCell = function(i,j) {
		return cells[i][j];
	}
	//добавляет набор ячеек
	this.putCells = function(cell) {
		cells.push(cell);
	}

	this.export = function() {
		var toExport = [];

		for(var i = 0; i < cells.length; i++) {
			toExport[i] = [];

			for(var j = 0; j < cells[i].length; j++) {
				toExport[i][j] = cells[i][j].options.content;
			}
		}
		// return cells
		return toExport;
	}

	this.min = function(fieldIndex) {
		var items = this.export();
		var result = {
			val : items[0][fieldIndex],
			line : 0
		}

		for(var i = 1; i < items.length; i++) {
			if(items[i][fieldIndex] < result.val) {
				result.val = items[i][fieldIndex];
				result.line = i;
			}
		}

		return result;
	}

	this.max = function(fieldIndex) {
		var items = this.export();

		var result = {
			val : items[0][fieldIndex],
			line : 0
		}

		for(var i = 1; i < items.length; i++) {
			if(items[i][fieldIndex] > result.val) {
				result.val = items[i][fieldIndex];
				result.line = i;
			}
		}

		return result;
	}

	this.init();
}
FinCalc.widgets.Table.prototype = new FinCalc.widgets.BaseWidget();



FinCalc.widgets.TableLine = function() {

	this.element = document.createElement('div');
	this.element.className = 'table-line';

}
FinCalc.widgets.TableLine.prototype = new FinCalc.widgets.BaseWidget();



FinCalc.widgets.Cell = function(options) {

	this.options = {};

	this.options = $.extend({
		'content' : '',
		'tag' : 'h3'
	},options || {});


	this.createNewElement = function() {
		this.element = document.createElement(this.options.tag);
		this.element.innerHTML = this.options.content;
	}

	this.getValue = function() {
		return this.options.content;
	}

	this.setValue = function(val) {
		this.element.innerHTML = this.options.content = val;
	}

	this.createNewElement();

	this.element.className = 'table-cell';

}
FinCalc.widgets.Cell.prototype = new FinCalc.widgets.BaseWidget();

FinCalc.widgets.Header = function(options) {

	this.options = $.extend({
		'content' : '',
		'tag' : 'h3'
	},options || {});

	this.element.className = 'table-header';

	this.createNewElement();


	this.element.className = 'table-header';
}
FinCalc.widgets.Header.prototype = new FinCalc.widgets.Cell();


FinCalc.widgets.InputTextFieldSingletone = function(saveOnEnter) {

	var self = this;
	this.cell = undefined;

	this.element = document.createElement('input');
	this.element.type = 'text';
	this.element.style.position = 'absolute';
	this.element.style.display = 'none';
	// this.hide();

	this.mutate = function(opts) {
		this.element.style.width = opts.width + 'px';
		this.element.style.height = opts.height + 'px';
		this.element.style.left = opts.x + 'px';
		this.element.style.top = opts.y + 'px';
		
		this.show();

		this.element.focus();

		return this;
	}

	this.hide = function() {
		this.element.style.display = 'none';
	}
	this.show = function() {
		this.element.style.	display = 'block';
	}

	this.setValue = function(value) {
		this.element.value = value;

		return this;
	}

	this.getValue = function() {
		return this.element.value;
	}

	this.setCell = function(cell) {
		this.cell = cell;
		this.setValue(cell.options.content);
	}
	this.getCell = function() {
		return this.cell;
	}

	document.body.appendChild(this.element);
	
	$(this.element).on('keydown blur', function(e) {
		if(e.which == 13 || e.type == 'blur') {
			self.cell.element.innerHTML = self.cell.options.content = self.getValue();
			self.hide();
		}
	});
}

FinCalc.widgets.InputTextFieldSingletone.prototype = new FinCalc.widgets.BaseWidget();
