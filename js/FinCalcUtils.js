
	FinCalc.util.LocalStorage = {
		save : function(name, value) {
			localStorage.setItem(name,JSON.stringify(value));
		},
		load : function(name) {
			// localStorage.clear();
			return JSON.parse(localStorage.getItem(name));
		},
		clear : function(name) {
			this.save(name,[[]]);
		}
	}

