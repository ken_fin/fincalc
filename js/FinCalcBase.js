var FinCalc = {
	app : {
		name : 'FinCalc'
	},
	widgets : {},
	util : {}
};

FinCalc.widgets.BaseWidget = function() {

	this.x = 0;
	this.y = 0;
	this.width = 0;
	this.height = 0;
	this.parent = undefined;
	this.element = undefined;

	// this.rootElement = document.body;

	this.init = function() {}

	this.render = function() {

		this.element.style = this.width;
		this.element.style = this.height;

		if(this.parent != undefined) {
			this.parent.appendChild(this.element);
		}
		else
			document.body.appendChild(this.element);

	}

	this.setParent = function(parentNode) {
		if(parentNode instanceof FinCalc.widgets.BaseWidget) {
			parentNode = parentNode.element;
		}
		this.parent = parentNode;
	}
}