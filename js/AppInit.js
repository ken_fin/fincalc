"use strict";

FinCalc.app.name = 'Калькулятор вкладов';

FinCalc.widgets.Table.prototype.highlight = function(line) {
	$(this.element).find('.highlight').removeClass('highlight');

	$(this.element)
		.find('.table-line')
		.eq(line+1)
		.addClass('highlight');
}


FinCalc.widgets.Table.prototype.getMinDaysByAmount = function(amount) {
	var items = this.export(),
		result = {
			line : 0,
			val : 0,
			profit : 0,
			days : 0
		};

	result.val = amount;

	for(var i = 0; i < items.length; i++) {

		var resultDeposit = parseInt(items[i][3]);
		var yearPercent = parseFloat(items[i][2]);
		
		var minDays = parseInt(items[i][4]);
		var elapsedTime = parseInt(items[i][4]);


		while(resultDeposit < amount) {
			var depositIncrement = (amount / 100) * yearPercent;
			elapsedTime += minDays;
			resultDeposit += depositIncrement;
		}

		// result message:
		var msg = resultDeposit.toFixed(2) + ' руб. за ' + elapsedTime + ' дн.';

		try{
			this.getCell(i,5).setValue(msg);
		}
		catch(e){
			var line = this.getCells(i);
			var cell = new FinCalc.widgets.Cell({content : msg})
			line.push(cell);
		}

		if(i == 0 || elapsedTime < result.days) {
			
			result.val = resultDeposit;
			result.days = elapsedTime;
			result.line = i;
			result.profit = 0;
		}

	}

	return result;
}

FinCalc.widgets.Table.prototype.getMaxAmountByDays = function(days) {

	var items = this.export(),
		result = {
			line : 0,
			val : 0,
			profit : 0,
			days : 0
		};

// 2 - проценты, 3 - мин. ставка, 4 - мин. кол-во дней
	result.days = days;

	for(var i = 0; i < items.length; i++) {
		// не берем поля с бОльшим кол-вом дней
		var minDays = parseInt(items[i][4]), //время начисления процентов
			totalDays = minDays + 0;

		var percent = parseFloat(items[i][2]),
			minDepo = parseFloat(items[i][3]),
			resDepo = minDepo + 0;

		if(days < minDays) {
			continue;
		}

		while(days > totalDays) {
			totalDays += minDays;

			var profit = (resDepo / minDays) * percent;

			resDepo += profit;
		}

		var msg = resDepo.toFixed(2) + ' руб. за ' + days + ' дн.';

		try{
			this.getCell(i,5).setValue(msg);
		}
		catch(e){
			var line = this.getCells(i);
			var cell = new FinCalc.widgets.Cell({content : msg});
			line.push(cell);
		}

		if(resDepo > result.val) {
			result.profit = 0;
			result.line = i;
			result.val = resDepo;
		}
	}

	return result;

}



$(function() {

	var data = FinCalc.util.LocalStorage.load(FinCalc.app.name) || [];

	var headers = ['Название банка', 'Вклад', 
	'Ставка (%)', 'Мин. сумма (руб.)', 'срок (дней)','Результат вычислений'];

	var table = new FinCalc.widgets.Table({
		'id' : 'application-container',
		headers : headers,
		// headers : ['id','name','value'],
		data : data
	});
	table.setParent(document.getElementById('page'));
	table.render();

	// console.log(table.export());

	document.getElementById('save').addEventListener('click', function(e) {
		FinCalc.util.LocalStorage.save(FinCalc.app.name, table.export());
	});

	document.getElementById('clear').addEventListener('click',function(e) {
		FinCalc.util.LocalStorage.clear(FinCalc.app.name);
	});

	document.getElementById('add').addEventListener('click', function(e) {
		var line = new FinCalc.widgets.TableLine();
		line.setParent(table);
		var cells = [];

		for(var i = 0; i < table.options.headers.length; i++) {
			var cell = new FinCalc.widgets.Cell({content:'---'});
			cell.setParent(line);
			cell.render();
			cells.push(cell);
		}
		table.putCells(cells);

		line.render();
	});

	//jquery callbacks:

	var tf = new FinCalc.widgets.InputTextFieldSingletone();
	tf.element.className = 'form-control';

	$('#application-container').delegate('.table-cell','click',function(e) {
		e.preventDefault();

		var cell = table.getCell($(this).parent().index()-1,$(this).index());
		var options = cell.options;

		tf.mutate({
			width : cell.element.offsetWidth,
			height : cell.element.offsetHeight,
			x : cell.element.offsetLeft,
			y : cell.element.offsetTop
		})
		.setCell(cell);
	});



	$('#calculate').click(function(e) {
		e.preventDefault();

		var mode = $('#mode').val();
		
		var item, msgText = 'Ошибка';

		if(mode == 1) {
			item = table.getMaxAmountByDays($('#days').val());
			msgText = 'Сумма за срок ' + item.days + ' дн. ~= ' + item.val;
		}
		else {
			item = table.getMinDaysByAmount($('#amount').val());
			msgText = 'Максимальная сумма за срок ' + item.days + ' дн. = ' + item.val;
		}
		
		table.highlight(item.line);

		$('#result').text(msgText);

	});



});
